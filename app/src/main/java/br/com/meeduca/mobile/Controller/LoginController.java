package br.com.meeduca.mobile.Controller;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import br.com.meeduca.mobile.Model.LoginModel;
import br.com.meeduca.mobile.NavigationActivity;
import br.com.meeduca.mobile.View.Cadastro;


    public class LoginController  {

        private Context contexto;

        private LoginModel loginModel;

        public LoginController(Context contexto) {
            this.contexto = contexto;

            loginModel = new LoginModel(this.contexto);
        }

        public void realizarLoginFacebook() {
            Log.i("LOGIN__", "Clicou no botão do Facebook!");
        }

        public void realizarLoginGoogle() {
            Log.i("LOGIN__", "Clicou no botão do Google!");
        }

        public void realizarLogin() {
            Intent tela = new Intent(contexto, NavigationActivity.class);
            contexto.startActivity(tela);
        }

        public void realizarEsqueceuSuaSenha() {
            Log.i("LOGIN__", "Clicou no texto de Esqueceu sua Senha?!");
        }

        public void realizarCadastro() {
            Intent tela = new Intent(contexto, Cadastro.class);
            contexto.startActivity(tela);
        }
    }
