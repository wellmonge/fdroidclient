package br.com.meeduca.mobile;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.meeduca.mobile.Common.Adapter.ToDoListAdapter;
import br.com.meeduca.mobile.Common.Dialog;
import br.com.meeduca.mobile.Model.ToDoListModel;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private ToDoListAdapter _adapter;

    private Context contexto;

    private Fragment _fragment;

    private CharSequence _title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        contexto = this;

        findViewById(R.id.next).setOnClickListener(this);

        findViewById(R.id.previous).setOnClickListener(this);

        findViewById(R.id.fab).setOnClickListener(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        int color = 0;

        ArrayList<ToDoListModel> todoList = new ArrayList<>();

        todoList.add(new ToDoListModel(this, "Lorem teste", "16:00:00","Etiqueta Aberta", 1 ){
        });
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",2));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta", 2));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",2));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",3));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",1));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",color));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",color));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",color));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",color));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",color));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",color));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",color));
        todoList.add(new ToDoListModel(this, "Lorem Ipsum", "16:00:00","Etiqueta Aberta",color));

        _adapter = new ToDoListAdapter(todoList, getApplicationContext()){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = super.getView(position, convertView, parent);
                final ToDoListModel item =  getItem(position);

                row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Dialog.showOKDialog(contexto, "Atividade", item.getTitle_event());
                    }
                });

                View divider = row.findViewById(R.id.divider_color);
                int sit =  item.getSituation();

                if(sit == 0)
                {
                    divider.setBackgroundColor(Color.GREEN);
                }
                else if (sit == 1)
                {
                    divider.setBackgroundColor(Color.RED);
                }
                else if (sit == 2)
                {
                    divider.setBackgroundColor(Color.GRAY);
                }
                else if (sit == 3)
                {
                    divider.setBackgroundColor(Color.YELLOW);
                }

                return row;
            }};

        ListView todoListView = findViewById(R.id.toDoListView);
        todoListView.setAdapter(_adapter);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(
                R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);

//        if (COUNT_NOTIFICATION > 0) {
//            ActionItemBadge.update(this, menu.findItem(R.id.btn_notify), getResources().getDrawable(R.drawable.ic_notification), ROSA, Constants.COUNT_NOTIFICATION);
//        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.btn_notify) {

            Dialog.showOKDialog(this, "Notification", "Notification screen");

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
         _title = item.getTitle();
         _fragment = new Fragment();

        FragmentManager supportedFragManager = getSupportFragmentManager();

        if (id == R.id.week) {
//            fragment = new WeekFragment();
//            Utils.navigateTo(_fragment,supportedFragManager);
            Dialog.showOKDialog(this, "Week", "todolist screen");

        }else  if (id == R.id.month) {
//            fragment = new FragmentFragment();
//            Utils.navigateTo(_fragment,supportedFragManager);

            Dialog.showOKDialog(this, "Month", "Month screen");
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(_title);
    }


    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.fab) {
            Snackbar.make(view, "...", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else if (id == R.id.next){

            Dialog.showOKDialog(this, "Next", "Next");
        } else if (id == R.id.previous){

            Dialog.showOKDialog(this, "Previous", "Previous");
        }

    }
}
