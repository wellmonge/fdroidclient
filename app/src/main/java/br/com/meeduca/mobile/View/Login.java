package br.com.meeduca.mobile.View;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.com.meeduca.mobile.Common.Dialog;
import br.com.meeduca.mobile.Controller.LoginController;
import br.com.meeduca.mobile.Main;
import br.com.meeduca.mobile.R;

public class Login extends Main implements View.OnClickListener {

    /** Variaveis padrão **/
    private Context contexto;

    private LoginController controller;

    /** Campos **/
    private TextView txtLorem;
    private Button btnLoginFacebook;
    private Button btnLoginGoogle;
    private EditText edtEmail;
    private EditText edtSenha;
    private TextView txtEsqueceuSuaSenha;
    private TextView txtCadastro;
    private Button btnContinuar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        contexto = this;

        controller = new LoginController(contexto);

        /** Instancia os campos **/
        carregaCampos();

        /** valida conectividade **/
        if (!conectividadeDisponivel()){
            Dialog.showOKDialog(contexto, "Sem conexão WI-FI", getString(R.string.offline_alert_message));
        }
    }

    protected boolean conectividadeDisponivel() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(contexto.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    protected void alteraCampoBackgroundColor(EditText e, Boolean b) {
        if (b) {
            e.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rounded_full_verde));
            e.setTextColor(getResources().getColor(R.color.padrao_verde));
        } else {
            e.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rounded_full_verde_claro));
            e.setTextColor(getResources().getColor(R.color.padrao_verde_claro));
        }
    }

    protected void carregaCampos() {
        txtLorem = (TextView) findViewById(R.id.txtLorem);
        btnLoginFacebook = (Button) findViewById(R.id.btnLoginFacebook);
        btnLoginGoogle = (Button) findViewById(R.id.btnLoginGoogle);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        txtEsqueceuSuaSenha = (TextView) findViewById(R.id.txtEsqueceuSuaSenha);
        txtCadastro = (TextView) findViewById(R.id.txtCadastro);
        btnContinuar = (Button) findViewById(R.id.btnContinuar);

        edtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                alteraCampoBackgroundColor(edtEmail, b);
            }
        });
        edtSenha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                alteraCampoBackgroundColor(edtSenha, b);
            }
        });

        btnLoginFacebook.setOnClickListener(this);
        btnLoginGoogle.setOnClickListener(this);
        btnContinuar.setOnClickListener(this);

        txtEsqueceuSuaSenha.setOnClickListener(this);
        txtCadastro.setOnClickListener(this);
    }

    private void realizarLoginFacebook() {
        controller.realizarLoginFacebook();
    }

    private void realizarLoginGoogle() {
        controller.realizarLoginGoogle();
    }

    private void realizarLogin() {

        controller.realizarLogin();
        if (edtEmail.getText().toString().length() <= 4 || !edtEmail.getText().toString().contains("@") || !edtEmail.getText().toString().contains(".")) {
            Log.i("LOGIN__", "Você precisa preencher o campos E-mail corretamente.");
        } else {
            controller.realizarLogin();
        }
    }

    private void realizarEsqueceuSuaSenha() {
        controller.realizarEsqueceuSuaSenha();
    }

    private void realizarCadastro() {
        controller.realizarCadastro();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLoginFacebook:
                realizarLoginFacebook();
                break;
            case R.id.btnLoginGoogle:
                realizarLoginGoogle();
                break;
            case R.id.btnContinuar:
                realizarLogin();
                break;
            case R.id.txtEsqueceuSuaSenha:
                realizarEsqueceuSuaSenha();
                break;
            case R.id.txtCadastro:
                realizarCadastro();
                break;
            default:
                break;
        }
    }
}
