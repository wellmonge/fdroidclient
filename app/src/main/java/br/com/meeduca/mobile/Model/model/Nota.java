package br.com.meeduca.mobile.Model.model;

public class Nota {

    private Integer id;
    private String nome;
    private String data;

    public Nota() { }

    public Nota(Integer id, String nome, String data) {
        this.id = id;
        this.nome = nome;
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
