package br.com.meeduca.mobile.View;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import br.com.meeduca.mobile.Common.Adapter.MeusCadernosAdapter;
import br.com.meeduca.mobile.Common.Adapter.MinhasNotasAdapter;
import br.com.meeduca.mobile.Controller.MeusCadernosController;
import br.com.meeduca.mobile.Model.model.Caderno;
import br.com.meeduca.mobile.Model.model.Nota;
import br.com.meeduca.mobile.R;

public class MinhasNotas extends AppCompatActivity {

    private Context contexto;
    private MeusCadernosController controller;

    private RecyclerView rvMinhasNotas;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Integer idCaderno = 0;
    private Caderno caderno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meu_caderno);

        contexto = this;
        controller = new MeusCadernosController(contexto);

        rvMinhasNotas = (RecyclerView) findViewById(R.id.rvMinhasNotas);

        rvMinhasNotas.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        rvMinhasNotas.setLayoutManager(mLayoutManager);

        idCaderno = getIntent().getIntExtra("CADERNO_ID", 0);

        buscarNotas();
    }

    protected void buscarNotas() {
        ArrayList<Nota> listaNotas = controller.buscarNotas(idCaderno);

        // specify an adapter (see also next example)
        mAdapter = new MinhasNotasAdapter(contexto, controller, listaNotas);
        rvMinhasNotas.setAdapter(mAdapter);
    }
}
