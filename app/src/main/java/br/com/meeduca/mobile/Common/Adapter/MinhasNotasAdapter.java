package br.com.meeduca.mobile.Common.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.meeduca.mobile.Controller.MeusCadernosController;
import br.com.meeduca.mobile.Model.model.Caderno;
import br.com.meeduca.mobile.Model.model.Nota;
import br.com.meeduca.mobile.R;

public class MinhasNotasAdapter extends RecyclerView.Adapter<MeusCadernosAdapter.MyViewHolder> {
    private Context contexto;
    private MeusCadernosController controller;
    private ArrayList<Nota> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtStatus;
        public TextView txtNome;
        public TextView txtData;
        public ImageView ivSetaDireita;



        public MyViewHolder(View v) {
            super(v);
            txtStatus = v.findViewById(R.id.txtStatus);
            txtNome = v.findViewById(R.id.txtNome);
            txtData = v.findViewById(R.id.txtData);
            ivSetaDireita = v.findViewById(R.id.ivSetaDireita);

        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MinhasNotasAdapter(Context pContexto, MeusCadernosController mController, ArrayList<Nota> myDataset) {
        contexto = pContexto;
        mDataset = myDataset;
        controller = mController;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MeusCadernosAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_minhas_notas, parent, false);

        MeusCadernosAdapter.MyViewHolder vh = new MeusCadernosAdapter.MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MeusCadernosAdapter.MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.txtStatus.setBackgroundColor(contexto.getResources().getColor(R.color.padrao_verde));
        holder.txtNome.setText(mDataset.get(position).getNome());
        holder.txtData.setText(mDataset.get(position).getData());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.mostraInformacoesCaderno(mDataset.get(position).getId());
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}