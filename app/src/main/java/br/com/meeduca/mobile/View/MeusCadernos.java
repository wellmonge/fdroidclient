package br.com.meeduca.mobile.View;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import br.com.meeduca.mobile.Common.Adapter.MeusCadernosAdapter;
import br.com.meeduca.mobile.Controller.MeusCadernosController;
import br.com.meeduca.mobile.Model.model.Caderno;
import br.com.meeduca.mobile.R;

public class MeusCadernos extends AppCompatActivity {

    private Context contexto;

    private RecyclerView rvMeusCadernos;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private MeusCadernosController controller;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meus_cadernos);

        contexto = this;
        controller = new MeusCadernosController(this);

        rvMeusCadernos = (RecyclerView) findViewById(R.id.rvMeusCadernos);

        rvMeusCadernos.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        rvMeusCadernos.setLayoutManager(mLayoutManager);

        buscarCadernos();

        // Set CustomAdapter as the adapter for RecyclerView.
        // recyclerView.adapter = BeerListAdapter(dataset)

        // return rootView
    }

    protected void buscarCadernos() {
        ArrayList<Caderno> listaCadernos = controller.buscarCadernos();

        // specify an adapter (see also next example)
        mAdapter = new MeusCadernosAdapter(contexto, controller, listaCadernos);
        rvMeusCadernos.setAdapter(mAdapter);
    }
}