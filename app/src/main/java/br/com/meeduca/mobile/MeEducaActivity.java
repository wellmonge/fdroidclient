package br.com.meeduca.mobile;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;

import br.com.meeduca.mobile.View.Login;
import br.com.meeduca.mobile.View.MeusCadernos;

public class MeEducaActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Intent i = new Intent(getBaseContext(), NavigationActivity.class);
        Intent i = new Intent(getBaseContext(), MeusCadernos.class);
        startActivity(i);
        finish();

    }

}