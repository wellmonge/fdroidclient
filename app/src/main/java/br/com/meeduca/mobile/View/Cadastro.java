package br.com.meeduca.mobile.View;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import br.com.meeduca.mobile.Common.Dialog;
import br.com.meeduca.mobile.Controller.CadastroController;
import br.com.meeduca.mobile.Main;
import br.com.meeduca.mobile.R;

public class Cadastro extends Main implements View.OnClickListener {

    /** Variaveis padrão **/
    private Context contexto;

    private CadastroController controller;

    /** Campos **/
    private ConstraintLayout clCadastro;
    private Button btnLoginFacebook;
    private Button btnLoginGoogle;
    private ImageView ivFoto;
    private TextView txtEditarFoto;
    private EditText edtNomeCompleto;
    private EditText edtDataNascimento;
    private ImageView ivCalendario;
    private Spinner spSexo;
    private EditText edtEmail;
    private EditText edtEmailConfirmar;
    private EditText edtSenha;
    private Spinner spInstituicaoEnsino;
    private Spinner spCurso;
    private Button btnContinuar;

    /** Variaveis de uso **/
    private Calendar calDataNascimento;
    private Calendar calDataNascimentoAuxiliar;
    private DatePickerDialog pickerDataNascimento;
    private DatePickerDialog.OnDateSetListener listenerDataNascimento;

    private Uri imgUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        contexto = this;

        controller = new CadastroController(contexto);

        /** Instancia os campos **/
        carregaCampos();

        /** valida conectividade **/
        if (!conectividadeDisponivel()){
            Dialog.showOKDialog(contexto, "Sem conexão WI-FI", getString(R.string.offline_alert_message));
        }
    }

    protected boolean conectividadeDisponivel() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(contexto.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    protected void alteraCampoBackgroundColor(EditText e, Boolean b) {
        if (b) {
            e.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rounded_full_verde));
            e.setTextColor(getResources().getColor(R.color.padrao_verde));
        } else {
            e.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rounded_full_verde_claro));
            e.setTextColor(getResources().getColor(R.color.padrao_verde_claro));
        }
    }

    protected void alteraFocusEditText(final EditText e) {
        e.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                alteraCampoBackgroundColor(e, b);
            }
        });
    }

    protected MaskTextWatcher mascaraEditText(EditText e, String m) {
        SimpleMaskFormatter smf = new SimpleMaskFormatter(m);
        MaskTextWatcher mtw = new MaskTextWatcher(e, smf);
        return mtw;
    }

    protected void carregaCampos() {
        clCadastro = (ConstraintLayout) findViewById(R.id.clCadastro);
        btnLoginFacebook = (Button) findViewById(R.id.btnLoginFacebook);
        btnLoginGoogle = (Button) findViewById(R.id.btnLoginGoogle);
        ivFoto = (ImageView) findViewById(R.id.ivFoto);
        txtEditarFoto = (TextView) findViewById(R.id.txtEditarFoto);
        edtNomeCompleto = (EditText) findViewById(R.id.edtNomeCompleto);
        edtDataNascimento = (EditText) findViewById(R.id.edtDataNascimento);
        ivCalendario = (ImageView) findViewById(R.id.ivCalendario);
        spSexo = (Spinner) findViewById(R.id.spSexo);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtEmailConfirmar = (EditText) findViewById(R.id.edtEmailConfirmar);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        spInstituicaoEnsino = (Spinner) findViewById(R.id.spInstituicaoEnsino);
        spCurso = (Spinner) findViewById(R.id.spCurso);
        btnContinuar = (Button) findViewById(R.id.btnContinuar);

        btnLoginFacebook.setOnClickListener(this);
        btnLoginGoogle.setOnClickListener(this);
        btnContinuar.setOnClickListener(this);

        ivFoto.setOnClickListener(this);
        txtEditarFoto.setOnClickListener(this);
        ivCalendario.setOnClickListener(this);

        for(int i = 0; i < clCadastro.getChildCount(); i++ )
            if(clCadastro.getChildAt(i) instanceof EditText ) {
                alteraFocusEditText((EditText) clCadastro.getChildAt(i));
            }

        edtDataNascimento.addTextChangedListener(mascaraEditText(edtDataNascimento, "NN/NN/NNNN"));

        calDataNascimento = Calendar.getInstance();
        calDataNascimento.add(Calendar.YEAR, -13);

        calDataNascimentoAuxiliar = Calendar.getInstance();

        edtDataNascimento.setText(String.format("%02d", calDataNascimento.get(Calendar.DAY_OF_MONTH))+"/"+String.format("%02d", (calDataNascimento.get(Calendar.MONTH) + 1))+"/"+calDataNascimento.get(Calendar.YEAR));

        // Listener
        listenerDataNascimento = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calDataNascimento.set(Calendar.YEAR, year);
                calDataNascimento.set(Calendar.MONTH, month);
                calDataNascimento.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String f = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(f, Locale.US);

                edtDataNascimento.setText(sdf.format(calDataNascimento.getTime()));
            }
        };

        // carregarInstituicaoEnsino();
        // carregarCursos(0);
    }

    protected void realizarCadastro() {
        Boolean continuar = true;

        if (edtNomeCompleto.getText().toString().equals("")) {
            Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_nome_completo));
            continuar = false;
        }

        if ((edtDataNascimento.getText().toString().equals("") || edtDataNascimento.getText().toString().equals("00/00/0000") || edtDataNascimento.getText().toString().length() < 10) && continuar) {
            Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento));
            continuar = false;
        } else {
            String[] dtNascimento = edtDataNascimento.getText().toString().split("/");

            if ((Integer.parseInt(dtNascimento[0]) <= 0 || Integer.parseInt(dtNascimento[0]) > 31) && continuar) {
                Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento_dia));
                continuar = false;
            }

            if ((Integer.parseInt(dtNascimento[1]) <= 0 || Integer.parseInt(dtNascimento[1]) > 12) && continuar) {
                Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento_mes));
                continuar = false;
            }

            if (Integer.parseInt(dtNascimento[2]) < (Calendar.getInstance().get(Calendar.YEAR) - 150) && continuar) {
                Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento_ano));
                continuar = false;
            }

            if (continuar) {
                // Configura o calendário auxiliar para verificar se a data é válida
                calDataNascimentoAuxiliar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dtNascimento[0].toString()));
                calDataNascimentoAuxiliar.set(Calendar.MONTH, Integer.parseInt(dtNascimento[1].toString()));
                calDataNascimentoAuxiliar.set(Calendar.YEAR, Integer.parseInt(dtNascimento[2].toString()));
                calDataNascimentoAuxiliar.set(Calendar.MONTH, calDataNascimentoAuxiliar.get(Calendar.MONTH) - 1);

                // Faz a verificação
                if ((calDataNascimentoAuxiliar.getTimeInMillis() > calDataNascimento.getTimeInMillis()) && continuar) {
                    Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento_anos));
                    continuar = false;
                }
            }
        }

        if ((edtEmail.getText().toString().length() <= 4 || !edtEmail.getText().toString().contains("@") || !edtEmail.getText().toString().contains(".")) && continuar) {
            Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_email));
            continuar = false;
        }

        if ((edtEmailConfirmar.getText().toString().length() <= 4 || !edtEmailConfirmar.getText().toString().contains("@") || !edtEmailConfirmar.getText().toString().contains(".")) && continuar) {
            Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_email_confirmar));
            continuar = false;
        } else {
            if ((!edtEmailConfirmar.getText().toString().equals(edtEmail.getText().toString())) && continuar) {
                Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_email_confirmar_igual));
                continuar = false;
            }
        }

        if (edtSenha.getText().toString().equals("") && continuar) {
            Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_senha));
            continuar = false;
        }

        /** Opcional
        if (spSexo.getSelectedItemPosition() == 0 && continuar) {
            Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_sexo));
            continuar = false;
        }
        */

        if (spInstituicaoEnsino.getSelectedItemPosition() == 0 && continuar) {
            Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_instituicao_ensino));
            continuar = false;
        }

        if (spCurso.getSelectedItemPosition() == 0 && continuar) {
            Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_curso));
            continuar = false;
        }

        // controller
        if (continuar) {
            try {
                JSONObject parametros = new JSONObject();
                parametros.put("nomeCompleto", edtNomeCompleto.getText().toString());
                parametros.put("dataNascimento", edtDataNascimento.getText().toString());
                // spSexo
                parametros.put("email", edtEmail.getText().toString());
                parametros.put("senha", edtSenha.getText().toString());
                // spInstituicao
                // spCurso
                // ivFoto

                controller.realizarCadastro(parametros);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    protected void abreModalEscolha() {
        LayoutInflater li = LayoutInflater.from(contexto);

        final View v = li.inflate(R.layout.dialog_escolher_imagem, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(contexto, R.style.AlertDialogTema);

        alertDialogBuilder.setView(v);

        // Cria o Alert Dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        // Instancia os campos
        final Button btnCamera = (Button) v.findViewById(R.id.btnCamera);
        final Button btnGaleria = (Button) v.findViewById(R.id.btnGaleria);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "Tire uma foto");
                values.put(MediaStore.Images.Media.DESCRIPTION, "");

                imgUri = contexto.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
                startActivityForResult(intent, 2);
            }
        });

        btnGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                Intent pickPhoto = new Intent();
                pickPhoto.setType("image/*");
                pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
                pickPhoto.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                startActivityForResult(Intent.createChooser(pickPhoto, "Selecione uma imagem"), 1);
            }
        });

        // Mostra o Alerta
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
    }

    private Uri getUri() {
        String state = Environment.getExternalStorageState();
        if(!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
            return MediaStore.Images.Media.INTERNAL_CONTENT_URI;

        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }

    /*

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            // Quando uma ou mais imagens são selecionadas da galeria
            if(requestCode == 1 && resultCode == RESULT_OK && data != null) {
                // Pega a imagem da variavel DATA
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                if(data.getData() != null) {
                    // Seleciona Uma imagem da galeria
                    Uri selectedImageUri = data.getData();
                    if (selectedImageUri != null) {
                        listaArquivos.add(new File(getPathSingleGallery(cntx, selectedImageUri)));
                        txtInserirAnexoTotal.setText("Total de Imagens: " + listaArquivos.size());
                        if (listaArquivos.size() > 0) {
                            ivInserirAnexoLimpar.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    if(data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        for(Integer i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();

                            String id = uri.getLastPathSegment().split(":")[1];
                            final String[] imageColumns = {MediaStore.Images.Media.DATA };
                            final String imageOrderBy = null;

                            Uri uri2 = getUri();
                            String selectedImagePath = "path";

                            Cursor imageCursor = managedQuery(uri2, imageColumns,MediaStore.Images.Media._ID + "="+id, null, imageOrderBy);

                            if (imageCursor.moveToFirst()) {
                                selectedImagePath = imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Images.Media.DATA));
                                File f = new File(selectedImagePath);
                                listaArquivos.add(f);
                            }
                        }

                        txtInserirAnexoTotal.setText("Total de Imagens: " + listaArquivos.size());
                        if (listaArquivos.size() > 0) {
                            ivInserirAnexoLimpar.setVisibility(View.VISIBLE);
                        }
                    }
                }
            } else if(requestCode == 2) {
                // Quando a imagem vem da camera
                Bitmap b = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);

                ArrayList<Bitmap> listaBMP = new ArrayList<>();
                listaBMP.add(b);

                AndroidNetworking.initialize(getApplicationContext());
                listaArquivos.add(new File(getRealPathFromUri(cntx, imgUri)));

                txtInserirAnexoTotal.setText("Total de Imagens: " + listaArquivos.size());
                if (listaArquivos.size() > 0) {
                    ivInserirAnexoLimpar.setVisibility(View.VISIBLE);
                }
            } else {
                Toast.makeText(this, "Você não selecionou nenhuma imagem.", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(cntx, "Erro inesperado! Por favor tente novamente dentro de instantes.", Toast.LENGTH_SHORT).show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
     */

    protected void abreCalendario() {
        pickerDataNascimento = new DatePickerDialog(
                contexto,
                R.style.TemaCalendarioVerde,
                listenerDataNascimento,
                calDataNascimento.get(Calendar.YEAR),
                calDataNascimento.get(Calendar.MONTH),
                calDataNascimento.get(Calendar.DAY_OF_MONTH)
        );

        pickerDataNascimento.setButton(DialogInterface.BUTTON_POSITIVE, "CONFIRMAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatePicker datePicker = pickerDataNascimento.getDatePicker();
                listenerDataNascimento.onDateSet(datePicker, datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
            }
        });
        pickerDataNascimento.getDatePicker().setMaxDate(calDataNascimento.getTimeInMillis());
        pickerDataNascimento.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLoginFacebook:
                // realizarLoginFacebook();
                break;
            case R.id.btnLoginGoogle:
                // realizarLoginFacebook();
                break;
            case R.id.btnContinuar:
                realizarCadastro();
                break;
            case R.id.ivFoto:
            case R.id.txtEditarFoto:
                abreModalEscolha();
                break;
            case R.id.ivCalendario:
                abreCalendario();
                break;
            default:
                break;
        }
    }
}
