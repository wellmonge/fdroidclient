package br.com.meeduca.mobile.Model.model;

public class Caderno {

    private Integer id = 0;
    private String nome = "";
    private String notas = "";
    private String data = "";
    private String status = "";

    public Caderno() { }

    public Caderno(Integer id, String nome, String notas, String data, String status) {
        this.id = id;
        this.nome = nome;
        this.notas = notas;
        this.data = data;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
