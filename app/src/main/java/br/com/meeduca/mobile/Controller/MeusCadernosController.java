package br.com.meeduca.mobile.Controller;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

import br.com.meeduca.mobile.Model.MeusCadernosModel;
import br.com.meeduca.mobile.Model.model.Caderno;
import br.com.meeduca.mobile.Model.model.Nota;
import br.com.meeduca.mobile.View.MinhasNotas;

public class MeusCadernosController {

    private Context contexto;

    private MeusCadernosModel model;

    public MeusCadernosController(Context contexto) {
        this.contexto = contexto;

        model = new MeusCadernosModel(this.contexto);
    }

    public ArrayList<Caderno> buscarCadernos() {
        return model.buscaMeusCadernos();
    }

    public void mostraInformacoesCaderno(Integer idCaderno) {
        Intent tela = new Intent(contexto, MinhasNotas.class);
        tela.putExtra("CADERNO_ID", idCaderno);
        contexto.startActivity(tela);
    }

    public ArrayList<Nota> buscarNotas(Integer idCaderno) {
        return model.buscaNotasPorIdCaderno(idCaderno);
    }
}