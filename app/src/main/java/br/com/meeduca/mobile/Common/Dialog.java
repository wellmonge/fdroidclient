package br.com.meeduca.mobile.Common;

import android.app.AlertDialog;
import android.content.Context;

public class Dialog {

    public static void showOKDialog(Context context, String title, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.show();
    }
}
