package br.com.meeduca.mobile.Common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import br.com.meeduca.mobile.R;

public class Utils extends AppCompatActivity {

    public static void showOKDialog(Context context, String title, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.show();
    }


    public static void navigateTo(Fragment fragment, FragmentManager fragmentManager) {
        String tag = fragment.getClass().getSimpleName();
        Fragment currentFragment = fragmentManager.findFragmentByTag(tag);
        if(currentFragment != null && currentFragment.getTag().equals(tag)){
            return;
        }
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.container, fragment, tag)
                .commit();
    }

    public static boolean validaPermissoes(int requestCode, Activity activity, String[] permissoes)
    {
        List<String> listaPermissoes = new ArrayList<>();

        if(Build.VERSION.SDK_INT >= 23) {
            for(String permissao: permissoes) {
                Boolean validaPermissao = ContextCompat.checkSelfPermission(activity, permissao) == PackageManager.PERMISSION_GRANTED;

                if(!validaPermissao) {
                    listaPermissoes.add(permissao);
                }
            }

            if(listaPermissoes.isEmpty()) return true;

            String[] novasPermissoes = new String[listaPermissoes.size()];
            listaPermissoes.toArray(novasPermissoes);

            ActivityCompat.requestPermissions(activity, novasPermissoes, requestCode);
        }
        return true;
    }

}
