package br.com.meeduca.mobile.Common.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.meeduca.mobile.Model.ToDoListModel;
import br.com.meeduca.mobile.R;
import br.com.meeduca.mobile.View.ToDoList;

public class ToDoListAdapter extends ArrayAdapter<ToDoListModel> implements View.OnClickListener{

    private ArrayList<ToDoListModel> dataSet;
    Context mContext;

    private static class ViewHolder {
        TextView txtName;
        TextView txtHourEvent;
        TextView txtItemInfo;
    }

    public ToDoListAdapter(ArrayList<ToDoListModel> data, Context context) {
        super(context, R.layout.todo_list_item, data);
        this.dataSet = data;
        this.mContext=context;
}

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        ToDoList dataModel=(ToDoList)object;

        switch (v.getId())
        {
            case R.id.item_info:

                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ToDoListModel dataModel = getItem(position);
        ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.todo_list_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.title_event);
            viewHolder.txtHourEvent = (TextView) convertView.findViewById(R.id.hour_event);
            viewHolder.txtItemInfo = (TextView) convertView.findViewById(R.id.item_info);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        viewHolder.txtName.setText(dataModel.getTitle_event());
        viewHolder.txtHourEvent.setText(dataModel.getHour_event());
        viewHolder.txtItemInfo.setText(dataModel.getItem_info());

        return convertView;
    }
}
