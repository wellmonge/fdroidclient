package br.com.meeduca.mobile.Model;

import android.content.Context;

import java.util.ArrayList;

import br.com.meeduca.mobile.Model.model.Caderno;
import br.com.meeduca.mobile.Model.model.Nota;

public class MeusCadernosModel {

    private Context contexto;

    public MeusCadernosModel(Context contexto) {
        this.contexto = contexto;
    }

    public ArrayList<Caderno> buscaMeusCadernos() {

        ArrayList<Caderno> listaCadernos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Caderno c = new Caderno(
                    i,
                    "Nome [" + i + "]",
                    i + " Notas",
                    i + "/10/2019",
                    "FINALIZADO"
            );
            listaCadernos.add(c);
        }

        return listaCadernos;
    }

    public ArrayList<Nota> buscaNotasPorIdCaderno(Integer idCaderno) {

        ArrayList<Nota> listaNotas = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Nota c = new Nota(
                    i,
                    "Nota [" + i + "]",
                    i + "/10/2019"
            );
            listaNotas.add(c);
        }

        return listaNotas;
    }
}
