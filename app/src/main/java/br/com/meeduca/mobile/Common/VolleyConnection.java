package br.com.meeduca.mobile.Common;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyConnection {

	private static final String TAG = VolleyConnection.class.getSimpleName();
	private RequestQueue requestQueue;
	public OnTaskCompleted delegate=null;

	public VolleyConnection(Context context){
		String userAgent = "volley/0";
		try {
			String packageName = context.getPackageName();
			PackageInfo info = context.getPackageManager().getPackageInfo(packageName, 0);
			userAgent = packageName + "/" + info.versionCode;
		} catch (PackageManager.NameNotFoundException e) {}
		HttpStack httpStack = new MyHttpClientStack(AndroidHttpClient.newInstance(userAgent));
		requestQueue = Volley.newRequestQueue(context, httpStack);
		//requestQueue = Volley.newRequestQueue(context);
	}

	public void connWsArray(final String url, int method, final JSONArray data, final String token, final String tag){System.setProperty("http.keepAlive", "false");
		Log.i(TAG, "data > " + (data != null ? data.toString() : "null"));
		Log.i(TAG, "url : " + url);
		JsonArrayRequest request;
		request = new JsonArrayRequest(method, url, data, new Response.Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				Log.i(TAG, "SUCCESS: " + response.toString());
				delegate.processFinish(response, tag);
			}}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				NetworkResponse networkResponse = error.networkResponse;
				Log.e(TAG, "ERROR: " + error.getMessage());
				JSONArray errArray = new JSONArray();
				JSONObject errObject = new JSONObject();
				try {
					errObject.put("error", error.getMessage());
					if(networkResponse != null) {
                        errObject.put("error", new String(networkResponse.data));
                        errObject.put("code", networkResponse.statusCode);
                    }
				} catch (JSONException e) {
					Log.e(TAG, e.getMessage());
				}
				delegate.processFinish(errArray.put(errObject), tag);
			}
		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<>();
				params.put("Content-Type", "application/json; charset=utf-8");
				params.put("x-Authorization", token);
				return params;
			}

			@Override
			public Priority getPriority(){
				return(Priority.NORMAL);
			}

		};
		request.setRetryPolicy(new DefaultRetryPolicy(60000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		requestQueue.add(request);
	}

	public void connWs(final String url, int method, final JSONObject data, final String token, final String tag){System.setProperty("http.keepAlive", "false");
		Log.i(TAG, "data > " + (data != null ? data.toString() : "null"));
		Log.i(TAG, "url : " + url);
		JsonObjectRequest request = new JsonObjectRequest(method, url, data, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				Log.i(TAG, "SUCCESS: " + response.toString());
				delegate.processFinish(response, tag);
			}}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				NetworkResponse networkResponse = error.networkResponse;
				Log.e(TAG, "ERROR: " + error.getMessage());
				JSONObject errObject = new JSONObject();
				try {
                    errObject.put("error", error.getMessage());
					if(networkResponse != null) {
                        errObject.put("error", new String(networkResponse.data));
						errObject.put("code", networkResponse.statusCode);
					}
				} catch (JSONException e) {
					Log.e(TAG, e.getMessage());
				}
				delegate.processFinish(errObject, tag);
			}
		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<>();
				params.put("Content-Type", "application/json; charset=utf-8");
				params.put("x-Authorization", token);
				return params;
			}

			@Override
			public Priority getPriority(){
				return(Priority.NORMAL);
			}

		};
		request.setRetryPolicy(new DefaultRetryPolicy(60000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		requestQueue.add(request);
	}

	public void cancelTagRequest(String tag){
		requestQueue.cancelAll(tag);
	}
}