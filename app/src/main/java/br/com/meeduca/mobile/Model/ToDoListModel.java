package br.com.meeduca.mobile.Model;

import android.content.Context;

public class ToDoListModel {

    private Context contexto;

    private String title_event;
    private String hour_event;
    private String item_info;
    private int situation;

    public String getTitle_event() {
        return title_event;
    }

    public void setTitle_event(String title_event) {
        this.title_event = title_event;
    }

    public String getHour_event() {
        return hour_event;
    }

    public void setHour_event(String hour_event) {
        this.hour_event = hour_event;
    }

    public String getItem_info() {
        return item_info;
    }

    public void setItem_info(String item_info) {
        this.item_info = item_info;
    }

    public ToDoListModel(Context contexto, String title_event, String hour_event, String item_info, int situation) {
        this.contexto = contexto;

        this.title_event=title_event;
        this.hour_event=hour_event;
        this.item_info=item_info;
        this.situation=situation;


}


    public int getSituation() {
        return situation;
    }

    public void setSituation(int situation) {
        this.situation = situation;
    }
}
