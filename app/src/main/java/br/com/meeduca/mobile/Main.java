package br.com.meeduca.mobile;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import br.com.meeduca.mobile.Common.Utils;

public class Main extends AppCompatActivity {

    private String[] permissoesNecessarias = {
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Valida as permissões
        Utils.validaPermissoes(1, this, permissoesNecessarias);
    }

    @Override
    public void setContentView(int layoutResID) {
        // View da Main
        DrawerLayout baseView = (DrawerLayout) getLayoutInflater().inflate(R.layout.main, null);

        // Container da View
        FrameLayout mainContent = (FrameLayout) baseView.findViewById(R.id.main_content);

        // Injeta o novo layout (layoutResID) no layout da main container
        getLayoutInflater().inflate(layoutResID, mainContent, true);

        // Chama o método super da classe pai
        super.setContentView(baseView);
    }
}
