package br.com.meeduca.mobile.View;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import br.com.meeduca.mobile.Common.Dialog;
import br.com.meeduca.mobile.Controller.EsqueceuSuaSenhaController;
import br.com.meeduca.mobile.Main;
import br.com.meeduca.mobile.R;

public class EsqueceuSuaSenha extends Main implements View.OnClickListener  {

    /** Variaveis padrão **/
    private Context contexto;

    private EsqueceuSuaSenhaController controller;

    /** Campos **/
    private EditText edtEmail;
    private EditText edtDataNascimento;
    private ImageView ivCalendario;
    private Button btnContinuar;

    /** Variaveis de uso **/
    private Calendar calDataNascimento;
    private Calendar calDataNascimentoAuxiliar;
    private DatePickerDialog pickerDataNascimento;
    private DatePickerDialog.OnDateSetListener listenerDataNascimento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esqueceu_sua_senha);

        contexto = this;

        controller = new EsqueceuSuaSenhaController(contexto);

        /** Instancia os campos **/
        carregaCampos();
    }

    protected void alteraCampoBackgroundColor(EditText e, Boolean b) {
        if (b) {
            e.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rounded_full_verde));
            e.setTextColor(getResources().getColor(R.color.padrao_verde));
        } else {
            e.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rounded_full_verde_claro));
            e.setTextColor(getResources().getColor(R.color.padrao_verde_claro));
        }
    }

    protected MaskTextWatcher mascaraEditText(EditText e, String m) {
        SimpleMaskFormatter smf = new SimpleMaskFormatter(m);
        MaskTextWatcher mtw = new MaskTextWatcher(e, smf);
        return mtw;
    }

    protected void carregaCampos() {
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtDataNascimento = (EditText) findViewById(R.id.edtDataNascimento);
        ivCalendario = (ImageView) findViewById(R.id.ivCalendario);
        btnContinuar = (Button) findViewById(R.id.btnContinuar);

        edtDataNascimento.addTextChangedListener(mascaraEditText(edtDataNascimento, "NN/NN/NNNN"));

        edtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                alteraCampoBackgroundColor(edtEmail, b);
            }
        });
        edtDataNascimento.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                alteraCampoBackgroundColor(edtDataNascimento, b);
            }
        });

        btnContinuar.setOnClickListener(this);
    }

    protected void abreCalendario() {
        pickerDataNascimento = new DatePickerDialog(
                contexto,
                R.style.TemaCalendarioVerde,
                listenerDataNascimento,
                calDataNascimento.get(Calendar.YEAR),
                calDataNascimento.get(Calendar.MONTH),
                calDataNascimento.get(Calendar.DAY_OF_MONTH)
        );

        pickerDataNascimento.setButton(DialogInterface.BUTTON_POSITIVE, "CONFIRMAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatePicker datePicker = pickerDataNascimento.getDatePicker();
                listenerDataNascimento.onDateSet(datePicker, datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
            }
        });
        pickerDataNascimento.getDatePicker().setMaxDate(calDataNascimento.getTimeInMillis());
        pickerDataNascimento.show();
    }

    private void realizarEsqueciSenha() {
        if (edtEmail.getText().toString().length() <= 4 || !edtEmail.getText().toString().contains("@") || !edtEmail.getText().toString().contains(".")) {
            Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_email));
        } else {
            Boolean continuar = true;

            if ((edtDataNascimento.getText().toString().equals("") || edtDataNascimento.getText().toString().equals("00/00/0000") || edtDataNascimento.getText().toString().length() < 10) && continuar) {
                Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento));
                continuar = false;
            } else {
                String[] dtNascimento = edtDataNascimento.getText().toString().split("/");

                if ((Integer.parseInt(dtNascimento[0]) <= 0 || Integer.parseInt(dtNascimento[0]) > 31) && continuar) {
                    Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento_dia));
                    continuar = false;
                }

                if ((Integer.parseInt(dtNascimento[1]) <= 0 || Integer.parseInt(dtNascimento[1]) > 12) && continuar) {
                    Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento_mes));
                    continuar = false;
                }

                if (Integer.parseInt(dtNascimento[2]) < (Calendar.getInstance().get(Calendar.YEAR) - 150) && continuar) {
                    Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento_ano));
                    continuar = false;
                }

                if (continuar) {
                    // Configura o calendário auxiliar para verificar se a data é válida
                    calDataNascimentoAuxiliar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dtNascimento[0].toString()));
                    calDataNascimentoAuxiliar.set(Calendar.MONTH, Integer.parseInt(dtNascimento[1].toString()));
                    calDataNascimentoAuxiliar.set(Calendar.YEAR, Integer.parseInt(dtNascimento[2].toString()));
                    calDataNascimentoAuxiliar.set(Calendar.MONTH, calDataNascimentoAuxiliar.get(Calendar.MONTH) - 1);

                    // Faz a verificação
                    if ((calDataNascimentoAuxiliar.getTimeInMillis() > calDataNascimento.getTimeInMillis()) && continuar) {
                        Dialog.showOKDialog(contexto, getString(R.string.oops), getString(R.string.oops_data_nascimento_anos));
                        continuar = false;
                    }
                }
            }

            if (continuar) {
                try {
                    JSONObject parametros = new JSONObject();
                    parametros.put("email", edtEmail.getText().toString());
                    parametros.put("dataNascimento", edtDataNascimento.getText().toString());

                    controller.realizarEsqueciSenha(parametros);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivCalendario:
                abreCalendario();
                break;
            case R.id.btnContinuar:
                realizarEsqueciSenha();
                break;
            default:
                break;
        }
    }
}
