package br.com.meeduca.mobile.Common;

import org.json.JSONArray;
import org.json.JSONObject;

public interface OnTaskCompleted {
	void processFinish(JSONObject output, String tag);
	void processFinish(JSONArray output, String tag);
}